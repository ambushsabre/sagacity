package  
{
	import org.flixel.*;
	/**
	 * ...
	 * @author Andrew Nissen
	 */
	public class Runner extends FlxSprite
	{
		[Embed(source = "../runner.png")] protected var Sprite:Class;
		private var isRight:Boolean = true;
		public function Runner(_x:int, _y:int) 
		{
			super(_x, _y);
			loadGraphic(Sprite, true, true , 8, 5);
			Registry.enemies.add(this);
			maxVelocity.x = 30;
			addAnimation("walk", [0, 1], 2, true);
			addAnimation("walkleft", [0, 2], 2, true);
			Registry.grpEntities.add(this);
			play("walk");
		}
		override public function update():void
		{
			acceleration.x = 0;
			if (isRight == true)
			{
				acceleration.x = maxVelocity.x*4;
			}
			if (isRight == false)
			{
				acceleration.x = -maxVelocity.x*4;
			}
			super.update();
		}
		
		private function restartLevel(param1:FlxObject, param2:FlxObject):void
		{
			Registry.editorSave = "";
			FlxG.switchState(new PlayState(Registry.currentLevel));
			Registry.zone = 1;
			FlxG.bgColor = 0xFF000000;
		}
		
		public function swap():void
		{
			if (isRight == true)
			{
				isRight = false;
				play("walkleft");
			}
			else if (isRight == false)
			{
				isRight = true;
				play("walk");
			}
		}
		
		public function save():void
		{
			if (this.alive == true)
			{
				var xreturn:String = "";
				var yreturn:String = "";
				xreturn += Math.round(x).toString();
				yreturn += Math.round(y).toString();
				if (x < 100)
				{
					xreturn = "";
					xreturn += "0" + Math.round(x).toString();
				}
				if (y < 100)
				{
					yreturn = "";
					yreturn += "0" + Math.round(y).toString();
				}
				Registry.editorSave += "r" + xreturn + yreturn + "-";
			}
		}
		
	}

}