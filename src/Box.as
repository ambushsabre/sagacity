package  
{
	import org.flixel.*;
	/**
	 * ...
	 * @author Andrew Nissen
	 */
	public class Box extends FlxSprite
	{
		[Embed(source = "../box.png")] protected var Sprite:Class;
		private var currentZone:int;
		private var currentGroup:FlxGroup = Registry.grpZone1;
		public function Box(_x:int, _y:int, zone:int) 
		{
			x = _x;
            y = _y;
			currentZone = zone;
			if (zone == 1)
				currentGroup = Registry.grpZone1;
			else if (zone == 2)
				currentGroup = Registry.grpZone2;
			else if (zone == 3)
				currentGroup = Registry.grpZone3;
			else if (zone == 4)
				currentGroup = Registry.grpZone4;
            loadGraphic(Sprite, true, false, 8, 8);
			maxVelocity.x = 80;
            maxVelocity.y = 175;
            acceleration.y = 150;
            acceleration.y = 150;
            drag.x = maxVelocity.x * 4;
			
			if (currentGroup != Registry.grpZone1)
				alpha = .5;
			currentGroup.add(this);
			
			Registry.grpEntities.add(this);
		}
		
		override public function update():void 
		{
			acceleration.x = 0;
			if (FlxG.keys.C)
			{
				currentZone = 1;
				currentGroup.remove(this);
				Registry.grpZone1.add(this);
			}
			if (Registry.zone == currentZone)
			{
				alpha = 1;
			}
			else
			{
				alpha = .5;
			}
			
		}
		
		public function save():void
		{
			if (this.alive == true)
			{
				var xreturn:String = "";
				var yreturn:String = "";
				xreturn += Math.round(x).toString();
				yreturn += Math.round(y).toString();
				if (x < 100)
				{
					xreturn = "";
					xreturn += "0" + Math.round(x).toString();
				}
				if (y < 100)
				{
					yreturn = "";
					yreturn += "0" + Math.round(y).toString();
				}
				Registry.editorSave += "b" + xreturn + yreturn + currentZone.toString() + "-";
			}
		}
		
	}

}