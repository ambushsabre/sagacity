package  
{
        import org.flixel.*;
        /**
         * ...
         * @author Andrew Nissen
         */
        public class Player extends FlxSprite
        {
                [Embed(source = "../guy.png")] protected var IMGplayer:Class;
                private var right:Boolean = true;
                //private var zone:int = 1;
				
				private var _jump:Number;
                public function Player(_x:int, _y:int) 
                {
                        x = _x;
                        y = _y;		
                        loadGraphic(IMGplayer, true, false, 8, 8);
						addAnimation("idle", [0], 1, false );
						addAnimation("jump", [1]);
						addAnimation("phase", [0, 2, 3, 4, 4,4,4, 0], 12, false);
						addAnimation("run", [0, 5, 6], 3, false);
						play("idle");
						
						
						//experimental jumping stuff
						acceleration.y = 150; //Set the gravity like you want
						maxVelocity.y = 175;
						maxVelocity.x = 80;
						acceleration.x = 150;
						drag.x = maxVelocity.x * 4;
                        
                }
                
                override public function update():void 
                {
                    acceleration.x = 0;
					if (Registry.editor.visible == false)
					{
						//this.
						acceleration.y = 150;
						if (FlxG.keys.LEFT)
						{
							acceleration.x = -maxVelocity.x * 4;
							right = false;
							this.facing = LEFT;
							play("run");
						}
						else if (FlxG.keys.RIGHT)
						{
							acceleration.x = maxVelocity.x * 4;
							right = true;
							this.facing = RIGHT;
							play("run");
						}
						else
							play("idle");
						
						 if((_jump >= 0) && (FlxG.keys.Z)) //You can also use space or any other key you want
						{
							_jump += FlxG.elapsed;
							if(_jump > 0.40) _jump = -1; //You can't jump for more than 0.25 seconds
						}
						else _jump = -1;
			 
						if (_jump > 0)
						{
							if(_jump < 0.065)
								velocity.y = -maxVelocity.y /2; //This is the minimum speed of the jump
							else
								acceleration.y = 75; //The general acceleration of the jump
						} else
						{
							velocity.y = 60;  //Make the character fall after the jump button is released
						}
						/*
						if (FlxG.keys.Z && this.isTouching(FlxObject.FLOOR))
						{
							velocity.y = -maxVelocity.y / 2;
						}
						//else
							//play("idle");*/
						if (this.isTouching(FlxObject.FLOOR))
						{
							_jump = 0;
						}
					}
					else
					{
						//this.allowCollisions = false;
						acceleration.y = 0;
						if (FlxG.keys.LEFT)
						{
							x -= 5;
						}
						if (FlxG.keys.RIGHT)
						{
							x += 5;
						}
						if (FlxG.keys.UP)
						{
							y -= 5;
						}
						if (FlxG.keys.DOWN)
						{
							y += 5;
						}
					}
                    if (FlxG.keys.justPressed("X"))
					{
						play("phase");
						//play("idle");
						if (Registry.zone > 3)
							Registry.zone = 1;
						else
							Registry.zone++;
							
						if (Registry.zone == 1)
						{
							FlxG.bgColor = 0xFF727272;
						}
						else if (Registry.zone == 2)
						{
							FlxG.bgColor = 0xFF00A750;
						}
						else if (Registry.zone == 3)
						{
							FlxG.bgColor = 0xFF800000;
						}
						else if (Registry.zone == 4)
						{
							FlxG.bgColor = 0xFF008080;
						}
						Registry.hudZone.text = Registry.zone.toString();
					}
						
					if (FlxG.keys.ONE)
					{
						Registry.editorItem = 1;
					}
					else if (FlxG.keys.TWO)
					{
						Registry.editorItem = 2;
					}
					else if (FlxG.keys.THREE)
					{
						Registry.editorItem = 3;
					}
					else if (FlxG.keys.FOUR)
					{
						Registry.editorItem = 4;
					}
					else if (FlxG.keys.FIVE)
					{
						Registry.editorItem = 5;
					}
					else if (FlxG.keys.SIX)
					{
						Registry.editorItem = 6;
					}
					else if (FlxG.keys.SEVEN)
					{
						Registry.editorItem = 7;
					}
					else if (FlxG.keys.ZERO)
					{
						Registry.editorItem = 0;
					}
						
					if (FlxG.keys.justPressed("E"))
					{
						if (Registry.editorEnabled == true)
						{
							if (Registry.editor.visible == false)
							{
								Registry.editor.visible = true;
								//FlxG.mouse.show();
							}
							else
							{
								Registry.editor.visible = false;
									//FlxG.mouse.hide();
							}
						}
					}
					if (FlxG.keys.justPressed("Q"))
					{
						trace("X: " + x.toString() + " Y: " + y.toString());
					}
                }
				
			public function save():void 
			{
				var xreturn:String = "";
				var yreturn:String = "";
				xreturn += Math.round(x).toString();
				yreturn += Math.round(y).toString();
				if (x < 100)
				{
					xreturn = "";
					xreturn += "0" + Math.round(x).toString();
				}
				if (y < 100)
				{
					yreturn = "";
					yreturn += "0" + Math.round(y).toString();
				}
				Registry.editorSave += "p" + xreturn + yreturn + Registry.zone.toString() + "-";
			}
        }

}