package
{
	import org.flixel.*;

	public class MenuState extends FlxState
	{
		
		private var playButton:FlxButton;
		private var devButton:FlxButton;
		private var menuLayer:int = 1;
		private var menuText:FlxText;
		[Embed(source = "../screen.png")] protected var IMGScreen:Class;
		override public function create():void
		{
			FlxG.bgColor = 0xFF727272;
			var lines:FlxSprite = new FlxSprite(0, 0, IMGScreen);
			lines.x = 160;
			lines.y = 135;
			lines.scrollFactor.x = 0;
			lines.scrollFactor.y = 0;
			add(lines);
			/*
			devButton = new FlxButton(FlxG.width/2-40,FlxG.height / 3 + 60, "User Levels", onSite);
			devButton.color = 0xffD4D943;
			devButton.label.color = 0xffD8EBA2;
			add(devButton);
			
			playButton = new FlxButton(FlxG.width/2-40,FlxG.height / 3 + 100, "Editor", onPlay);
			playButton.color = devButton.color;
			playButton.label.color = devButton.label.color;
			//playButton.
			//playButton.scale = new FlxPoint(.5, .5);
			add(playButton);
			*/
			menuText = new FlxText(FlxG.width / 2 - 13, FlxG.height / 3 + 75, 200, "Play");
			//menuText.size = 12;
			add(menuText);
			
			FlxG.mouse.show();
			FlxG.camera.x -= 480;
            FlxG.camera.y -= 404;
			
			var helpText:FlxText = new FlxText(FlxG.width / 2 - 80, FlxG.height / 3 + 120, 200, "(Z)");
			helpText.size = 8;
			add(helpText);
			FlxKongregate.init(apiHasLoaded);
			
		}
		
		private function apiHasLoaded():void 
		{
			FlxKongregate.connect();
			FlxKongregate.isLocal = false;
		}
		
		override public function update():void
		{
			if (FlxG.keys.justPressed("UP"))
			{ 
				menuLayer++;
				if (menuLayer > 4)
					menuLayer = 1;
				trace(menuLayer);
			}
			else if (FlxG.keys.justPressed("DOWN"))
			{
				menuLayer -= 1;
				if (menuLayer <= 0)
					menuLayer = 4;
				trace(menuLayer);
			}
				if (menuLayer == 1)
				{
					menuText.text = "Play";
					menuText.x = FlxG.width / 2 - 13;
					FlxG.bgColor = 0xFF727272;
					//trace(Registry.levels[0]);
				}
				else if (menuLayer == 2)
				{
					FlxG.bgColor = 0xFF00A750;
					menuText.x = FlxG.width / 2 - 15;
					menuText.text = "Editor";
				}
				else if (menuLayer == 3)
				{
					FlxG.bgColor = 0xFF800000;
					menuText.x = FlxG.width / 2 - 30;
					menuText.text = "User Levels";
				}
				else if (menuLayer == 4)
				{
					FlxG.bgColor = 0xFF008080;
					menuText.x = FlxG.width / 2 - 56;
					menuText.text = "Made by Andrew Nissen\n\tadnissen.com\n\t@ambushsabre";
				}
			if (FlxG.keys.justPressed("Z"))
			{
				//Eventually the first one will take the player to the "stock" levels
				if (menuLayer == 1)
					onPlay();
				else if (menuLayer == 2)
					onEdit();
				else if (menuLayer == 3)
					onLevels();
			}
			super.update();	
		}
		
		protected function onLevels():void
		{
			Registry.editorEnabled = false;
			FlxKongregate.browseSharedContent("level");
			FlxKongregate.addLoadListener("level", menuLoad);
		}
		
		protected function menuLoad(loadedObject:Object):void 
		{
			FlxG.switchState(new PlayState(loadedObject.content));
		}
		protected function onEdit():void 
		{
			Registry.isLocal = false;
			FlxG.bgColor = 0xFF727272;
			Registry.editorEnabled = true;
			FlxG.switchState(new PlayState("s0521621-v0602512-v0682512-v0762512-v0842512-v0922512-l0442341-l0362341-l0282341-l0202341-l0122341-l0522342-l0522422-l0522502-l0602343-l0682343-v1002512-v1082512-v1162512-v1242512-v1322512-v1402512-v1482512-v1562512-v1642512-l0972344-l1052344-l1132344-l1472354-l1212111-l0932032-l0651963-l0931814-"));
		}
		protected function onPlay():void
		{
			//Fills the array with all the levels
			Registry.levels[0] = "l0732491-l0732411-l1172492-l1172412-l1172332-l1562493-l1562413-l1562333-l1562253-v1482513-v1402513-v1322513-v1252513-l1932244-l2012244-l2092244-l2172244-l2252244-k2272154-w0222254-s0102451-";
			Registry.levels[1] = "l0742491-l0742411-l0742331-l0742251-l1142492-l1142412-l1142332-l1142252-l1142172-l1492493-l1492413-l1492333-l1492253-l1492173-l1492093-b1822484-l1972081-l2052081-l2132081-l2212081-s2101971-w2412251-w2411931-w2411611-";
			Registry.levels[2] = "s0521621-v0602512-v0682512-v0762512-v0842512-v0922512-l0282341-l0522342-l0522422-l0522502-l0602343-l0682343-v1002512-v1082512-v1162512-v1242512-v1322512-v1402512-v1482512-v1562512-v1642512-l0972344-l1052344-l1132344-l1472354-l1212111-l0932032-l0651963-l0931814-";
			Registry.totalLevels = 3;
			Registry.isLocal = true;
			FlxG.bgColor = 0xFF727272;
			Registry.editorEnabled = false;
			//make sure that this actually loads the factory levels instead of a new one
			FlxG.switchState(new PlayState(Registry.levels[Registry.levelNum]));
		}
		
		protected function onOver():void
		{

		}
		
	}
}

