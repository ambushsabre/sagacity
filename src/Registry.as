package  
{
	import org.flixel.FlxGroup;
	import org.flixel.FlxText;
	/**
	 * ...
	 * @author Andrew Nissen
	 */
	public class Registry 
	{
		//Stats and internal game state stuff
		public static var isLocal:Boolean;
		public static var levels:Array = new Array();
		public static var levelNum:int = 0;
		public static var totalLevels:int = 0;
		
		public static var COLOR1:uint = 0xFF727272;;
		public static var COLOR2:uint;
		public static var COLOR3:uint;
		public static var COLOR4:uint;
		
		public static var zone:int = 1;
		public static var player:Player;
		public static var playerHasKey:Boolean = false;
		public static var currentLevel:String;
		
		public static var grpEntities:FlxGroup;
		
		public static var grpAll:FlxGroup;
		public static var grpZone1:FlxGroup;
		public static var grpZone2:FlxGroup;
		public static var grpZone3:FlxGroup;
		public static var grpZone4:FlxGroup;
		
		public static var enemies:FlxGroup;
		
		public static var hudZone:FlxText;
		
		//Editor stuff
		public static var editor:Editor;
		//1: Yellow Wall
		//2: Runner Dude
		//3: Star
		//4: Box
		//5: Block
		public static var editorItem:int = 0;
		//a large string that contains all the data for the level
		public static var editorSave:String = "";
		public static var editorEnabled:Boolean = true;
		
		public function Registry() 
		{
		}
		
	}

}