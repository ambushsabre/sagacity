package  
{
	import org.flixel.*;
	public class Loading extends FlxState 
	{
		
		public function Loading(newLevel:String):void 
		{
			FlxG.switchState(new PlayState(Registry.levels[Registry.levelNum]));
		}
		
	}

}