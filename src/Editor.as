package  
{
	/**
	 * ...
	 * @author Andrew Nissen
	 */
	import org.flixel.*;
	public class Editor extends FlxSprite
	{
		[Embed(source = "../wall.png")] protected var spriteWall:Class;
		[Embed(source = "../runnereditor.png")] protected var spriteRunner:Class;
		[Embed(source = "../editHighlight.png")] protected var spriteHighlight:Class;
		[Embed(source = "../box.png")] protected var spriteBox:Class;
		[Embed(source = "../block.png")] protected var spriteBlock:Class;
		[Embed(source = "../star.png")] protected var spriteStar:Class;
		[Embed(source = "../key.png")] protected var spriteKey:Class;
		[Embed(source = "../spikes.png")] protected var spriteSpike:Class;
		public function Editor() 
		{
			this.visible = false;
		}
		
		override public function update():void 
		{
			//These are rounded so that since saving doesn't support floats, 
			//things will be placed exactly where you see them
			x = Math.round(FlxG.mouse.x);
			y = Math.round(FlxG.mouse.y);
			
			 if (Registry.editorItem == 1)
			{
				loadGraphic(spriteWall, false, false, 4, 32);
			}
			else if (Registry.editorItem == 2)
			{
				loadGraphic(spriteRunner, false, false, 8, 5);
			}
			else if (Registry.editorItem == 3)
			{
				loadGraphic(spriteStar, false, false, 8, 8);
			}
			else if (Registry.editorItem == 4)
			{
				loadGraphic(spriteBox, false, false, 8, 8);
			}
			else if (Registry.editorItem == 5)
			{
				loadGraphic(spriteBlock, false, false, 8, 8);
			}
			else if (Registry.editorItem == 6)
			{
				loadGraphic(spriteKey, false, false, 8, 8);
			}
			else if (Registry.editorItem == 7)
			{
				loadGraphic(spriteSpike, false, false, 8, 6);
			}
			else if (Registry.editorItem == 0)
			{
				loadGraphic(spriteHighlight, false, false, 8, 8);
				FlxG.overlap(this, Registry.grpEntities, editorDel);
			}
		}
		
		private function editorDel(player:FlxSprite, object2:FlxSprite):void 
		{
			if (FlxG.mouse.justPressed())
			{
				object2.kill();
			}
		}
		
	}

}