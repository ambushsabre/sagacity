package  
{
	import org.flixel.*;
	/**
	 * ...
	 * @author Andrew Nissen
	 */
	public class Star extends FlxSprite
	{
		[Embed(source = "../star.png")] protected var Sprite:Class;
		private var currentZone:int;
		private var startx:int;
		private var starty:int;
		private var currentGroup:FlxGroup = Registry.grpZone1;
		public function Star(_x:int, _y:int, zone:int) 
		{
			x = _x;
            y = _y;
			currentZone = zone;
			if (zone == 1)
				currentGroup = Registry.grpZone1;
			else if (zone == 2)
				currentGroup = Registry.grpZone2;
			else if (zone == 3)
				currentGroup = Registry.grpZone3;
			else if (zone == 4)
				currentGroup = Registry.grpZone4;
            loadGraphic(Sprite, true, false, 8, 8);
			maxVelocity.x = 0;
            maxVelocity.y = 0;
            acceleration.y = 0;
            acceleration.y = 0;
			immovable = true;
			if (currentGroup != Registry.grpZone1)
				alpha = .5;
			currentGroup.add(this);
			
			this.immovable = true;
			Registry.grpEntities.add(this);
		}
		
		override public function update():void 
		{
			FlxG.collide(this, Registry.player, endLevel);
			acceleration.x = 0;
			if (FlxG.keys.C)
			{
				currentZone = 1;
				currentGroup.remove(this);
				Registry.grpZone1.add(this);
			}
			if (Registry.zone == currentZone)
			{
				visible = true;
				alpha = 1;
			}
			else
			{
				alpha = .5;
			}
			
		}
		
		private function endLevel(obj1:FlxObject, obj2:FlxObject):void 
		{
			
			if (Registry.isLocal == false)
			{
				FlxKongregate.browseSharedContent("level");
				FlxKongregate.addLoadListener("level", menuLoad);
			}
			else if (Registry.levelNum + 1< Registry.totalLevels)
			{
				Registry.levelNum++;
				Registry.zone = 1;
				FlxG.bgColor = Registry.COLOR1;
				FlxG.switchState(new PlayState(Registry.levels[Registry.levelNum]));
			}
			else 
			{	
				FlxG.switchState(new MenuState);
			}
		}
		private function menuLoad(loadedObject:Object):void 
		{
			FlxG.switchState(new PlayState(loadedObject.content));
		}
		public function save():void 
		{
			if (this.alive == true)
			{
				var xreturn:String = "";
				var yreturn:String = "";
				xreturn += Math.round(x).toString();
				yreturn += Math.round(y).toString();
				if (x < 100)
				{
					xreturn = "";
					xreturn += "0" + Math.round(x).toString();
				}
				if (y < 100)
				{
					yreturn = "";
					yreturn += "0" + Math.round(y).toString();
				}
				Registry.editorSave += "s" + xreturn + yreturn + currentZone.toString() + "-";
			}
		}
		
	}

}