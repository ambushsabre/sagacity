package
{

	import org.flixel.*;

	public class PlayState extends FlxState
	{
		private var level:FlxTilemap;
		private var loadLevel:String;
		[Embed(source = "../screen.png")] protected var IMGScreen:Class;
		public function PlayState(kongLevel:String):void 
		{
			loadLevel = kongLevel;
			//create(loadLevel);
		}
		override public function create():void
		{
			FlxG.mouse.hide();
			//an empty level, so that all the blocks make up the level and not the tilemap, because they're smaller to store
			var levelData:Array = new Array(
				1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
				1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1,
				1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1,
				1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1,
				1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1,
				1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1,
				1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1,
				1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1,
				1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1,
				1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1,
				1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1,
				1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1,
				1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1,
				1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1,
				1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1,
				1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1,
				1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1,
				1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1,
				1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1,
				1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1,
				1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1,
				1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1,
				1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1,
				1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1,
				1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1,
				1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1,
				1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1,
				1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1,
				1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1,
				1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1,
				1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1,
				1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1,
				1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 );
			
			level = new FlxTilemap();
			level.loadMap(FlxTilemap.arrayToCSV(levelData, 40), FlxTilemap.ImgAuto,0,0,FlxTilemap.AUTO);
			add(level);
			
			var lines:FlxSprite = new FlxSprite(0, 0, IMGScreen);
			lines.x = 160;
			lines.y = 87;
			lines.scrollFactor.x = 0;
			lines.scrollFactor.y = 0;
			add(lines);
			//a simple string that shows what layer the player is currently on
			Registry.hudZone = new FlxText(0, 0, 100, Registry.zone.toString());
			Registry.hudZone.scrollFactor.x = 0;
			Registry.hudZone.scrollFactor.y = 0;
			
			add(Registry.hudZone);
			
			Registry.grpEntities = new FlxGroup();
			
			//Adding the player
			Registry.player = new Player(40, 210);
			add(Registry.player);
			
			//Each layer is it's own group, and entities move themselves around when needed
			//This way, it's easy to control which things the player is colliding with, as seen below
			Registry.grpZone1 = new FlxGroup(0);
			Registry.grpZone2 = new FlxGroup(0);
			Registry.grpZone3 = new FlxGroup(0);
			Registry.grpZone4 = new FlxGroup(0);
			
			Registry.enemies = new FlxGroup();
			Registry.grpAll = new FlxGroup(0);
			Registry.grpAll.add(Registry.grpZone1);
			Registry.grpAll.add(Registry.grpZone2);
			Registry.grpAll.add(Registry.grpZone3);
			Registry.grpAll.add(Registry.grpZone4);
			
			makeLevel(loadLevel);
			Registry.currentLevel = loadLevel;
			Registry.editor = new Editor();
			add(Registry.editor);
			FlxG.camera.x -= 480;
            FlxG.camera.y -= 260;
			

		}
		
		override public function update():void 
		{
			FlxG.camera.follow(Registry.player, FlxCamera.STYLE_PLATFORMER);
			FlxG.collide(Registry.player, level);
			FlxG.collide(Registry.enemies, level, RunCollide);
			FlxG.collide(Registry.grpAll, level);
			FlxG.collide(Registry.grpZone1, Registry.grpZone1);
			FlxG.collide(Registry.grpZone2, Registry.grpZone2);
			FlxG.collide(Registry.grpZone3, Registry.grpZone3);
			FlxG.collide(Registry.grpZone4, Registry.grpZone4);
			
			if (FlxG.collide(Registry.player, Registry.enemies))
			{
				restartLevel();
			}
			if (Registry.zone == 1)
			{
				FlxG.collide(Registry.player, Registry.grpZone1);
				FlxG.collide(Registry.enemies, Registry.grpZone1, RunCollide);
			}
			else if (Registry.zone == 2)
			{
				FlxG.collide(Registry.player, Registry.grpZone2);
				FlxG.collide(Registry.enemies, Registry.grpZone2, RunCollide);
			}
			else if (Registry.zone == 3)
			{
				FlxG.collide(Registry.player, Registry.grpZone3);
				FlxG.collide(Registry.enemies, Registry.grpZone3, RunCollide);
			}
			else if (Registry.zone == 4)
			{
				FlxG.collide(Registry.player, Registry.grpZone4);
				FlxG.collide(Registry.enemies, Registry.grpZone4, RunCollide);
			}
			if (FlxG.mouse.justPressed())
			{
				if (Registry.editorItem == 1)
					add(new Wall(Math.round(FlxG.mouse.x), Math.round(FlxG.mouse.y), Registry.zone));
				else if (Registry.editorItem == 2)
					add(new Runner(Math.round(FlxG.mouse.x), Math.round(FlxG.mouse.y)));
				else if (Registry.editorItem == 3)
					add(new Star(Math.round(FlxG.mouse.x), Math.round(FlxG.mouse.y), Registry.zone));
				else if (Registry.editorItem == 4)
					add(new Box(Math.round(FlxG.mouse.x), Math.round(FlxG.mouse.y), Registry.zone));
				else if (Registry.editorItem == 5)
					add(new Block(Math.round(FlxG.mouse.x), Math.round(FlxG.mouse.y), Registry.zone));
				else if (Registry.editorItem == 6)
					add(new Key(Math.round(FlxG.mouse.x), Math.round(FlxG.mouse.y), Registry.zone));
				else if (Registry.editorItem == 7)
					add(new Spike(Math.round(FlxG.mouse.x), Math.round(FlxG.mouse.y), Registry.zone));
					
				Registry.currentLevel = saveLevel();
			}
			if (FlxG.keys.justPressed("R"))
			{
				restartLevel();
			}
			if (FlxG.keys.justPressed("L"))
			{
				FlxKongregate.browseSharedContent("level");
				FlxKongregate.addLoadListener("level", menuLoad);
			}

			if (FlxG.keys.justPressed("S"))
			{
				FlxKongregate.saveSharedContent("level", saveLevel(), saveSent);
			}
			
			if (FlxG.keys.ESCAPE)
			{
				FlxG.switchState(new MenuState);
			}
			
			super.update();
		}
		private function restartLevel():void 
		{
			Registry.playerHasKey = false;
			Registry.editorSave = "";
			trace(Registry.currentLevel);
			FlxG.switchState(new PlayState(Registry.currentLevel));
			Registry.zone = 1;
			FlxG.bgColor = 0xFF727272;
			this.exists = false;
		}
		private function menuLoad(loadedObject:Object):void 
		{
			FlxG.switchState(new PlayState(loadedObject.content));
		}
		private function saveSent():void
		{
			loadLevel = saveLevel();
			Registry.currentLevel = loadLevel;
		}
		
		private function makeLevel(input:String):void
		{
			for (var i:int = 0; i < input.length; i++)
			{
				if (input.substring(i, i +1) != "-")
				{
					//boxes
					if (input.substring(i, i + 1) == "b")
						add(new Box(int(input.substring(i + 1, i + 4)), int(input.substring(i + 4, i + 7)), int(input.substring(i + 7, i + 8))));
					//Keys
					if (input.substring(i, i + 1) == "k")
						add(new Key(int(input.substring(i + 1, i + 4)), int(input.substring(i + 4, i + 7)), int(input.substring(i + 7, i + 8))));
					//yellow walls
					if (input.substring(i, i + 1) == "w")
						add(new Wall(int(input.substring(i + 1, i + 4)), int(input.substring(i + 4, i + 7)), int(input.substring(i + 7, i + 8))));
					//pink blocks
					if (input.substring(i, i + 1) == "l")
						add(new Block(int(input.substring(i + 1, i + 4)), int(input.substring(i + 4, i + 7)), int(input.substring(i + 7, i + 8))));
					if (input.substring(i, i + 1) == "v")
						add(new Spike(int(input.substring(i + 1, i + 4)), int(input.substring(i + 4, i + 7)), int(input.substring(i + 7, i + 8))));
					//Star
					if (input.substring(i, i + 1) == "s")
						add(new Star(int(input.substring(i + 1, i + 4)), int(input.substring(i + 4, i + 7)), int(input.substring(i + 7, i + 8))));
					//runner duder
					if (input.substring(i, i + 1) == "r")
						add(new Runner(int(input.substring(i + 1, i + 4)), int(input.substring(i + 4, i + 7))));
					//Saves the players location
					if (input.substring(i, i + 1) == "p")
					{
						Registry.player.x = int(input.substring(i + 1, i + 4));
						Registry.player.y = int(input.substring(i + 4, i + 7));
					}
				}
			}
		}
		
		public function saveLevel():String
		{
			Registry.editorSave = "";
			Registry.grpEntities.callAll("save");
			return Registry.editorSave;
		}
		public function RunCollide(Sprite1:Runner, Sprite2:FlxObject):void
		{
			Sprite1.swap();
		}
		
	}
}

